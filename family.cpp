//#define WINDOWS
#define LINUX

/** FAMILY TREE */

#include<iostream>
#include<fstream>
#include<string.h>
#include<stdlib.h>

using namespace std;

struct node
{
    char name[50];
    short int age,x;    // x - height of node
    bool g;             // g- gender
    node* fc;           // Pointer to first child
    node* ns;           // Pointer to next sibiling

    node();
    void getData();
};

node::node()
{
    fc=ns=NULL;
    g=0;
    strcpy(name,"");
    age=x=0;
}

void node::getData()
{
    char ch;
    cout<<"\nName of the Person: ";
    cin>>name;
    cout<<"Age of "<<name<<": ";
    cin>>age;
    cout<<name<<" is (m/f): ";
    cin>>ch;
    if(ch=='m')
        g=1;
}

class familyTree
{

public:

    node* start;

    familyTree();

    node* traverseDown(node*,char[]);   // Search functions
    node* traverseRight(node*,char[]);
    node* search(char[]);

    void addSib(node*,node*);           // Functions for adding new members
    void addChild(node*,node*);
    void addNew();
    void addNewNonInteractive(char[], char[], char[], char[], char[]);
    void find();                        // Function to find relations
    void dfs(node*, ofstream &fout, char[], char[]);
    void show(node*);                   // Function to show details of particular person
    void display(node*, int);                // Function to display full tree
    void destroy(node*);                // Function to destroy full tree
    void updateX(node*,int);

    void readFromFile();
    void writeToFile();
};

familyTree::familyTree()
{
    start = NULL;
}

void familyTree::destroy(node* ptr)
{
    node* temp = ptr;

    if(ptr==NULL)
        return;

    while(ptr!=NULL)
    {
        destroy(ptr->fc);
        temp = ptr;
        ptr = ptr->ns;
        delete temp;
    }

    start = NULL;
}

//This function displays the individual node (person) information
void familyTree::show(node* ptr)
{
    char g[10];
    strcpy(g,"Female");
    if(ptr->g)
        strcpy(g,"Male");
    cout<<"\n\nName: "<< ptr->name <<endl;
    cout<<"Age: "<< ptr->age <<endl;
    cout<<"Gender: "<<g<<endl;
    if(ptr->g)
    {
        cout<<"Gender: Male"<<endl;
    }
    else
    {
        cout<<"Gender: Female"<<endl;
    }
    
}

void familyTree::display(node* ptr, int endln=1)
{
    // Traverses the full n-array tree by recursion to display names of all people

    if(ptr==NULL)
        return;

    while(ptr!=NULL)
    {
        if(endln)
            cout<< ptr->name <<endl;
        else
            cout<< ptr->name <<", ";
        display(ptr->fc, endln);
        ptr = ptr->ns;
    }
}


void familyTree::dfs(node* ptr, ofstream &fout, char relative[50], char relation[10])
{
    // Traverses the full n-array tree by depth first search to gather node info of all people
        
    if(ptr==NULL)
        return;

    while(ptr!=NULL)
    {
        fout << ":beg:" <<endl;
        fout << ptr->name <<endl;
        fout << ptr->age <<endl;
        if(ptr->g)
            fout << "m" <<endl;
        else
            fout << "f" << endl;
        if(strcmp(relative,"NULL")==0)
        {
            fout << "NULL" << endl;
            fout << "NULL" << endl;
        }
        else
        {
            fout << relative << endl;
            fout << relation << endl;
        }
        strcpy(relation,"child");
        dfs(ptr->fc, fout, ptr->name, relation);
        ptr = ptr->ns;
        relative = ptr->name;
        strcpy(relation,"sibling");
    }
}

void familyTree::updateX(node* ptr,int x)
{
    // Traverses the full n-array tree by recursion and updates x value of all people

    if(ptr==NULL)
        return;

    while(ptr!=NULL)
    {
        updateX(ptr->fc,x++);
        if(ptr->ns!=NULL)
            ptr->ns->x = x;
        ptr = ptr->ns;
    }
}

//finds the relationship between two person
void familyTree::find()
{

    /*
        Same hight: Sibiling if same parent, else cousin
        Difference of height = 1 - Parent if immediate, else uncule/aunt
        Difference oh height = 2 - Grand parents if same link, elss idk
    */

    char name1[50],name2[50];
    cout<<"Enter names of two persons:\n";
    cin>>name1>>name2;
    node* ptr1 = search(name1);
    node* ptr2 = search(name2);
    node* ptr;
    //node* ptrk=ptr1;
    //node* ptrk1=ptr2;

    switch(ptr1->x - ptr2->x)
    {

    case 0:
            char s[50];
            strcpy(s,"sister");
            if(ptr1->g)
                strcpy(s,"brother");

            ptr = ptr1;
            while(ptr!=NULL)
            {
                if(ptr==ptr2)
                {
                    cout<<endl<<name1<<" is "<<name2<<"'s "<<s<<endl;
                    return;
                }
                cout<<ptr;
                ptr = ptr->ns;
                cout<<ptr;
            }
            ptr = ptr2;
            while(ptr!=NULL)
            {
                if(ptr==ptr1)
                {
                    cout<<endl<<name1<<" is "<<name2<<"'s "<<s<<endl;
                    return;
                }
                cout<<ptr;
                ptr = ptr->ns;
                cout<<ptr;
            }
            cout<<endl<<name1<<" and "<<name2<<" are Cousins";
            break;

    case 1:
            char str3[50];
            strcpy(str3,"daughter");
            if(ptr1->g)
                strcpy(str3,"son");
            ptr2 = ptr2->fc;
            while(ptr2!=NULL)
            {
                if(ptr2==ptr1)
                {
                    cout<<endl<<name1<<" is "<<name2<<"'s "<<str3;
                    return;
                }
                ptr2=ptr2->ns;
            }
            strcpy(str3,"niece");
            if(ptr1->g)
                strcpy(str3,"nephew");
            cout<<endl<<name1<<" is "<<name2<<"'s "<<str3;
            break;
    case -1:
            char str[10];
            strcpy(str,"mother");
            if(ptr1->g)
                strcpy(str,"father");

            ptr = ptr1->fc;
            while(ptr!=NULL)
            {
                if(ptr==ptr2)
                {
                    cout<<endl<<name1<<" is "<<name2<<"'s "<<str;
                    return;
                }
                ptr=ptr->ns;
            }
            strcpy(str,"aunt");
            if(ptr1->g)
                strcpy(str,"uncle");
            cout<<endl<<name1<<" is "<<name2<<"'s "<<str;
            break;

    case 2:
            char str1[50];
            strcpy(str1,"daughter");
            if(ptr2->g)
                strcpy(str1,"son");
            ptr2 = ptr2->fc->fc;
            while(ptr2!=NULL)
            {
                if(ptr2==ptr1)
                {
                    cout<<endl<<name1<<" is grand "<<str1<<" of "<<name2;
                    return;
                }
                ptr2 = ptr2->ns;
            }
            break;
    case -2:
            char str2[50];
            strcpy(str2,"mother");

            if(ptr1->g)
                strcpy(str2,"father");

             ptr1 = ptr1->fc->fc;

            while(ptr1!=NULL)
            {
                if(ptr1==ptr2)
                {
                    cout<<endl<<name1<<" is grand "<<str2<<" of "<<name2;
                    return;
                }
                ptr1 = ptr1->ns;
            }

            break;
    default:
            cout<<"Too far relationship";
            break;
    }
}



node* familyTree::search(char s[50])
{
    /*
        Searches for the given name from start to it's sibilings and their children
        Returns the pointer of node where the name is present
    */

    node *ptr = start;
    node *sibling_ptr;
    node *child_ptr;
    if(strcmp(ptr->name,s)==0)
        return ptr;
    sibling_ptr = traverseRight(start,s);
    if(sibling_ptr!=NULL)
        return sibling_ptr;
    child_ptr = traverseDown(start,s);
    if(child_ptr!=NULL)
        return child_ptr;
    return NULL;
}

node* familyTree::traverseDown(node* ptr, char s[50])
{
    // Searches all the children

    ptr = ptr->fc;

    while(ptr!=NULL)
    {
        node *sibling_ptr;
        if(strcmp(ptr->name,s)==0 )
            return ptr;
        sibling_ptr = traverseRight(ptr,s);
        if(sibling_ptr!=NULL)
            return sibling_ptr;
        ptr = ptr->fc;
    }
    return NULL;
}

node* familyTree::traverseRight(node* ptr, char s[50])
{

    //  Searches all the sibilings

    ptr = ptr->ns;

    while(ptr!=NULL)
    {
        node *child_ptr;
        if(strcmp(ptr->name,s)==0)
            return ptr;
        child_ptr = traverseDown(ptr,s);
        if(child_ptr!=NULL)
            return child_ptr;
        ptr = ptr->ns;
    }
    return NULL;
}

void familyTree::addNew()
{
    node* temp = new node;
    temp->getData();

    if(start == NULL)
    {
        start = temp;
        temp->x=0;
    }

    else
    {
        cout<<"\nEnter any relation's name: ";
        char name[50];
        cin>>name;
        cout<<"\n1. Child\n2. Sibiling\n\n"<< temp->name <<" is ____ to "<<name<<" : ";
        int opt;
        cin>>opt;
        switch(opt)
        {
            case 1:
                    addChild(search(name),temp);
                    break;
            case 2:
                    addSib(search(name),temp);
                    break;

        }
    }
    cout<<"\nPerson sucessfully added.\n";
}

void familyTree::addNewNonInteractive(char name[50], char age[3], char gender[1], char relative[50], char relation[10])
{
    node* temp = new node;
    if(start == NULL)
    {
        start = temp;
        temp->x=0;
    }
    strcpy(temp->name,name);
    temp->age = atoi(age);
    if(strcmp(gender,"m")==0)
        temp->g = 1;
    else
        temp->g = 0;
    if(strcmp(relation, "NULL")!=0)
    {
        if(strcmp(relation,"child")==0)
            addChild(search(relative),temp);
        else
            addSib(search(relative),temp);
    }
    cout<< "\n" << name <<" sucessfully added.\n";
}

void familyTree::addSib(node* a,node* b)
{
    // b is added as sibling of a

    while(a->ns!=NULL)
        a=a->ns;
    a->ns = b;

    b->x = a->x;
}

void familyTree::addChild(node* a,node* b)
{

    // b is added as child as a (or) b is added as sibiling to last child of a

    if(a->fc==NULL)
        a->fc = b;
    else
        addSib(a->fc,b);

    b->x = a->x+1;
}

void connect(familyTree *T1, familyTree *T2)
{
    char name[50];
    int opt;
    int x;
    cout<<"Name of person in 1st tree to merge: ";
    cin>>name;
    cout<<T2->start->name<<" is __ to "<<name<<"\n1. Child 2. Sibling - ";;
    cin>>opt;
    node *ptr = T1->search(name);
    switch(opt)
    {
        case 1:
            T1->addChild(ptr,T2->start);
            x = ptr->x + 1;
            break;
        case 2:
            T1->addSib(ptr,T2->start);
            x = ptr->x;
            break;
     }
     T2->updateX(T2->start,x);
     //T2->destroy(T2->start);
     cout<<"\nMerged\n";
}

void familyTree::readFromFile()
{
    //reads family tree from a fiile
    char rfile[150];
    cout<<"Enter file name to read with absolute path: ";
    cin>>rfile;
    // Creation of ifstream class object to read the file 
    ifstream fin;
    char line[50];
    // by default open mode = ios::in mode 
    fin.open(rfile); 
  
    int is_start = 0;
    char gender[1];
    char age[3];
    char relation[10];
    char name[50];
    char relative[50];
    int index = 0;
    
// Execute a loop until EOF (End of File) 
    while(fin) { 
  
        // Read a Line from File
        fin >> line;

        if(strcmp(line,":beg:")==0)
        {
            is_start = 1;
            index = 0;
            continue;
        }
            
        if(strcmp(line,":end:")==0)
        {
            addNewNonInteractive(name, age, gender, relative, relation);
            is_start = 0;
            continue;
        }
            
        if(is_start)
        {
            switch(index)
            {
                case 0:
                    strcpy(name,line);
                    index++;
                    break;
                case 1:
                    strcpy(age,line);
                    index++;
                    break;
                case 2:
                    strcpy(gender,line);
                    index++;
                    break;
                case 3:
                    strcpy(relative,line);
                    index++;
                    break;
                case 4:
                    strcpy(relation,line);
                    index++;
                    break;
             }
             continue;
        }
    } 
  
    // Close the file 
    fin.close(); 
}

void familyTree::writeToFile()
{
    // Traverses the full n-array tree by recursion to display names of all people

    //writes current family tree into a file
    char wfile[150];
    char relative[50];
    char relation[10];
    cout<<"Enter file name to write with absolute path: ";
    cin>>wfile;
    // Creation of ofstream class object 
    ofstream fout;
    // by default ios::out mode, automatically deletes 
    // the content of file. To append the content, open in ios:app 
    // fout.open(wfile, ios::app) 
    fout.open(wfile);
  
    //do a depth first search and write the content to file
    strcpy(relative, "NULL");
    strcpy(relation, "NULL");
    dfs(start, fout, relative, relation);
    
    // Close the File 
    fout.close(); 

}

int main()
{
    familyTree T[100];
    int opt,n,n1,n2;
    char c,name[50];
    cout<<"\nEnter the family tree number = ";
    cin>>n;
    while(1)
    {
#ifdef WINDOWS
        system("cls");
#endif // WINDOWS
#ifdef LINUX
        system("clear");
#endif // LINUX
        cout<<"\n\n\n\tFamily tree no = "<<n<<"\n\n\t1. Add new person\n\t2. Find relationship b/w two persons\n\t3. Search\n\t4. Destroy\n\t5. Display\n\t6. Change family tree\n\t7. Connect two family trees\n\t8. Read family tree from File\n\t9. Write family tree to File\n\t10. Exit\n\n\tEnter your choice = ";
        cin>>opt;
        cout<<endl;

        switch(opt)
        {

        default:
                cout<<"Invalid input";
                break;

        case 1:
                T[n].addNew();
                break;

        case 2:
                T[n].find();
                break;

        case 3:
                cout<<"Enter name of person to search: ";
                cin>>name;
                T[n].show(T[n].search(name));
                break;

        case 4:
                T[n].destroy(T[n].start);
                cout<<"Tree "<<n<<" has been destroyed sucessfully";
                break;

        case 5:
                T[n].display(T[n].start,0);
                cout<<endl;
                break;

        case 6:
                cout<<"Enter family tree number: ";
                cin>>n;
                break;

        case 7:
               cout<<"Merge __ to __ \n";
               cin>>n2>>n1;
               connect(&T[n1],&T[n2]);
               break;

        case 8:
                T[n].readFromFile();
                break;

        case 9:
                T[n].writeToFile();
                break;

        case 10:
            return 0;

        }
        cout<<"\n\nPress any key to continue.....";
        cin>>c;
    }
}